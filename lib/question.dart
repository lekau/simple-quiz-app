import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Question extends StatelessWidget {
  final String _questionText;
  
  Question(this._questionText);

  @override 
  Widget build(BuildContext context) {
    return Container( 
      width: double.infinity,
      margin: EdgeInsets.all(30),
      child:Text(_questionText, 
        style: TextStyle(fontSize: 30), 
        textAlign: TextAlign.center),
    );
  }

}