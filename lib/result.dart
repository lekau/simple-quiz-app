import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final int resultScore;
  final Function resetHandler;
  Result(this.resultScore, this.resetHandler);

  String get resultPhrase {
    String resultText;
    if (resultScore == 30) {
      resultText = 'You are as Cool as Lekau, Welcome to the top!!!';
    } else if (resultScore > 20) {
      resultText = 'You not bad, we can be friends.';
    } else if (resultScore > 10) {
      resultText = 'Hai, second round interview.';
    } else {
      resultText = 'You really suck hey, i dont want to know you...';
    }
    return resultText;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Column(
      children: <Widget>[
        Text(
          resultPhrase,
          style: TextStyle(fontSize: 36, fontWeight: FontWeight.bold),
          textAlign: TextAlign.center,
        ),
        TextButton(onPressed: resetHandler, child: Text('Reset Quiz')),
      ],
    ));
  }
}
