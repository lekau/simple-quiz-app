import 'package:flutter/material.dart';

import './quiz.dart';
import './result.dart';

void main() => runApp(new SimpleQuizApp());

class SimpleQuizApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SimpleQuizAppState();
  }
}

class _SimpleQuizAppState extends State<SimpleQuizApp> {
  var _questionIndex = 0;
  var _totalScore = 0;
  var _questions = [
    {
      'questionText': 'What is your favourite car brand?',
      'answers': [
        {'text': 'BMW', 'score': 10},
        {'text': 'VW', 'score': 3},
        {'text': 'AUDI', 'score': 5},
        {'text': 'BENZ', 'score': 1}
      ]
    },
    {
      'questionText': 'Whats is your favourite car?',
      'answers': [
        {'text': 'M2', 'score': 10},
        {'text': 'GTI', 'score': 5},
        {'text': 'RS3', 'score': 1},
        {'text': 'A45', 'score': 3}
      ]
    },
    {
      'questionText': 'What colour must the car be',
      'answers': [
        {'text': 'BLACK', 'score': 5},
        {'text': 'BLUE', 'score': 3},
        {'text': 'WHITE', 'score': 10},
        {'text': 'RED', 'score': 1}
      ]
    }
  ];

  void _answerQuestion(int score) {
    setState(() {
      _questionIndex = _questionIndex + 1;
      _totalScore += score;
    });
  }

  void _resetQuiz() {
    setState(() {
      _questionIndex = 0;
      _totalScore = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Simple Quiz App'),
          centerTitle: true,
          backgroundColor: Colors.blueAccent,
        ),
        body: _questionIndex < _questions.length
            ? Quiz(
                questionIndex: _questionIndex,
                answerQuestion: _answerQuestion,
                questions: _questions,
              )
            : Result(_totalScore, _resetQuiz),
      ),
    );
  }
}
