import 'package:flutter/material.dart';
import './question.dart';
import './answerButton.dart';

class Quiz extends StatelessWidget {
  final List<Map<String, Object>> questions;
  final Function answerQuestion;
  final int questionIndex;

  Quiz({
    @required this.questions, 
    @required this.questionIndex, 
    @required this.answerQuestion});
  
  @override
  Widget build(BuildContext context) {
    return Column(
          children: [
            Question(questions.elementAt(questionIndex)['questionText']),
            ...(questions[questionIndex]['answers'] as List<Map<String,Object>>)
                .map((answer) {
              return AnswerButton(answer['text'], 
                () => answerQuestion(answer['score']));
            }).toList()
          ],
        );
  }
}
