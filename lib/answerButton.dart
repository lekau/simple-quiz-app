import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class AnswerButton extends StatelessWidget {
  final String buttonText;
  final Function selectHandler;
  AnswerButton(this.buttonText, this.selectHandler);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.all(5),
      child: ElevatedButton(
        child: Text(buttonText),
        onPressed: selectHandler,
        style: ElevatedButton.styleFrom(primary: Colors.green),
      ),
    );
  }
}
